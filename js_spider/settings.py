BOT_NAME = "js_spider"

SPIDER_MODULES = ["js_spider.spiders"]
NEWSPIDER_MODULE = "js_spider.spiders"


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Scrapy-Challenge-2 (17mayanksinghal@gmail.com)"

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

SPLASH_URL = "http://localhost:8050"
SPIDER_MIDDLEWARES = {
    "scrapy_splash.SplashDeduplicateArgsMiddleware": 100,
}


DOWNLOADER_MIDDLEWARES = {
    "scrapy_splash.SplashCookiesMiddleware": 723,
    "scrapy_splash.SplashMiddleware": 725,
    "scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware": 810,
}

DUPEFILTER_CLASS = "scrapy_splash.SplashAwareDupeFilter"

FEEDS = {
    "result.jl": {
        "format": "jsonlines",  # To support large data set
        "encoding": "utf8",
        "store_empty": False,
        "overwrite": True,
    },
}

SPIDER_MIDDLEWARES = {"scrapy_autounit.AutounitMiddleware": 950}

# Only enable when fixtures are to be updated
# AUTOUNIT_ENABLED = True
