import scrapy
from scrapy_splash import SplashRequest
from js_spider.items import QuoteItem


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    allowed_domains = ["quotes.toscrape.com"]
    start_urls = ["http://quotes.toscrape.com/js"]

    def start_requests(self):  # pragma: no cover
        for url in self.start_urls:
            yield SplashRequest(url, self.parse)

    def parse(self, response):
        for quote_card in response.css(".quote"):
            quote = QuoteItem()
            quote["author"] = quote_card.css("small.author::text").extract_first()

            # The quote body has Left & Right double quotes, which do not constitute data
            quote["quote"] = quote_card.css(".text::text").extract_first().strip("“”")

            quote["tags"] = quote_card.css(".tag::text").extract()
            yield quote

        next_page_url = response.css("li.next > a::attr(href)").extract_first()
        next_page_url = response.urljoin(next_page_url)
        if next_page_url:
            yield SplashRequest(next_page_url, callback=self.parse)
